const portfolio = require('express').Router();
const timeout = require('connect-timeout')

portfolio.get('/', timeout('6s'), (req, res) => {

	if (req.timedout) return res.status(408).json({mensagem: 'timeout'})

	res.render('index')
})

module.exports = portfolio;