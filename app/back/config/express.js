const app = require ('express')();
const express = require ('express');
const routes = require ('../routes');
const path = require ('path');
const ejs = require ('ejs');
const cors = require('cors')
const RateLimit = require('express-rate-limit');

mensage = 'Muitas tentativas foram feitas, tente novamente mais tarde'

const accountLimiter = new RateLimit({
    windowMs: 60*5000,
    max: 15,
    statusCode: 429,
    onLimitReached: (_, res) => res.status(429).json({message:mensage}),
    message: mensage
})

const publicPath = path.resolve(__dirname, '..', '..', '..') + '/public'
const frontPath = path.resolve(__dirname, '..', '..') + '/front'

app.use('/static', express.static(publicPath, {
    etag: false
}))

app.use(accountLimiter)

app.set('views', frontPath)

app.use(cors())

// EJS Engine
app.engine('html', ejs.renderFile);
app.set('view engine', 'html')

// Routes
app.use('', routes)

module.exports = app;