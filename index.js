const app = require('./app/back/config/express');

const port = 80

app.listen(port,'0.0.0.0', () => console.log(`server started on port ${port}`));

module.exports = app;